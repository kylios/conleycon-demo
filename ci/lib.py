import markdown
import os.path
import re


class BadFileNameException(Exception):
    def __init__(self, filename):
        super(BadFileNameException, self).__init__(
            'Expected format "YYYY-MM-DD_rest_of_filename.md", '
            'but got "' + filename + '"')


class BadFileContentsException(Exception):
    def __init__(self, file_path):
        super(BadFileContentsException, self).__init__(
            'File was not valid markdown: "' + file_path + '"')


def get_posts_path():
    script_path = os.path.realpath(__file__)
    script_dir = os.path.dirname(script_path)
    return os.path.join(script_dir, '../posts')


def parse_filename(filename):
    '''
    Validate that the file name is formatted like
    YYYY-MM-DD_blah_blah_blah.md and return its
    parts.

    Raises
        BadFileNameException

    Returns
        2-tuple where the first element is the date and the
        second element is the title as parsed from the filename.
    '''
    match = re.match(r'^(\d{4}-\d{2}-\d{2})_(.*)\.md$', filename)
    if not match:
        raise BadFileNameException(filename)

    date = match.group(1)
    title = match.group(2).replace('_', ' ').title()

    return date, title


def get_post_contents(file_path):
    '''
    Parse the file as markdown and return its contents.

    raises
        BadFileContensException

    returns
        The file contents after converted to html
    '''
    try:
        return markdown.markdown(open(file_path).read())
    except UnicodeDecodeError:
        raise BadFileContentsException(file_path)
