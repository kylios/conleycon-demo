#!/usr/bin/env python


import jinja2
import lib
import os


def main():
    posts_path = lib.get_posts_path()

    posts = list()
    for filename in sorted(os.listdir(posts_path), reverse=True):
        date, title = lib.parse_filename(filename)

        file_path = os.path.join(posts_path, filename)
        content = lib.get_post_contents(file_path)

        posts.append({
            'date': date,
            'title': title,
            'content': content
        })

    template = jinja2.Template(open('index.html.jinja2').read())
    generated_content = template.render(posts=posts)

    print(generated_content)


if __name__ == '__main__':
    main()