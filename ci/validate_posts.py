#!/usr/bin/env python

import os
import os.path
import re
import sys

import lib


def main():
    '''
    Loops through all the posts and ensures that their filenames
    conform and that they contain valid, parseable markdown
    content.
    '''
    posts_path = lib.get_posts_path()

    for filename in os.listdir(posts_path):

        print("Checking " + filename + " ...")
        lib.parse_filename(filename)

        file_path = os.path.join(posts_path, filename)
        lib.get_post_contents(file_path)

        print("  Ok")

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        print(e.message)
        sys.exit(1)
    else:
        sys.exit(0)