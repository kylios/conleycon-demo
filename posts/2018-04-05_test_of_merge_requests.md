Testing out merge requests, CI in branches, at the last minute.

Markdown is cool for a few reasons:

* bulletted lists
* `inline code snippets`
* **bold** and _italics_
* also:


     ____________________ 
    < Gitlab is the shit >
     -------------------- 
            \   ^__^
             \  (**)\_______
                (__)\       )\/\
                 U  ||----w |
                    ||     ||


